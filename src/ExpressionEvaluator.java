public interface ExpressionEvaluator {

    /**
     * Evaluates mathematical expression and returns a correct integer result.
     * For the expression <pre>1+3-2+4</pre> the method returns integer result 6.
     *
     * @param expression A valid mathematical expression (two operators in row are not allowed).
     * Each char of the input can be a either a positive one-digit number (such as 0 to 9) or an operator (+, -).
     *                   Other operations such as multiplication or division is not supported.
     *                   Float numbers are not supported.
     * @return integer value representing the result of the expression
     * @throws IllegalArgumentException if the expression contains invalid characters (letters, spaces, invalid operators, ...).
     */
    int evaluate(String expression);
}
