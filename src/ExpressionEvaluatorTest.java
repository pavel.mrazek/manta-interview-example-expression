import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ExpressionEvaluatorTest {
    private static final ExpressionEvaluator evaluator = new ExpressionEvaluatorImpl();

    @Test
    public void test1(){
        final Integer expectedResult = 8;
        final String expression = "8";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test2(){
        final Integer expectedResult = -9;
        final String expression = "0-9";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test3(){
        final Integer expectedResult = 1;
        final String expression = "1+0";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test4(){
        final Integer expectedResult = 4;
        final String expression = "2+1+1";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test5(){
        final Integer expectedResult = 0;
        final String expression = "3-3";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test6(){
        final Integer expectedResult = -1;
        final String expression = "5-2-4";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test7(){
        final Integer expectedResult = 15;
        final String expression = "6+1-4+2+0+7+3";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }

    @Test
    public void test8(){
        final Integer expectedResult = 0;
        final String expression = "5-5+5-5+5-5";
        assertEquals(expectedResult, Integer.valueOf(evaluator.evaluate(expression)));
    }


    @Test(expected = IllegalArgumentException.class)
    public void test9(){
        final String expression = "5*2";
        evaluator.evaluate(expression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test10(){
        final String expression = "1+(3-4)";
        evaluator.evaluate(expression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test11(){
        final String expression = "1+A";
        evaluator.evaluate(expression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test12(){
        final String expression = "1*3";
        evaluator.evaluate(expression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test13(){
        final String expression = "8/4";
        evaluator.evaluate(expression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test14(){
        final String expression = "7 -6";
        evaluator.evaluate(expression);
    }

}
